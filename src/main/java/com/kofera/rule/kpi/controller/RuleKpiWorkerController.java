package com.kofera.rule.kpi.controller;

import com.kofera.rule.kpi.task.RuleKpiPublisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RuleKpiWorkerController {
    private RuleKpiPublisher ruleKpiPublisher;

    @Autowired
    public RuleKpiWorkerController(RuleKpiPublisher ruleKpiPublisher) {
        this.ruleKpiPublisher = ruleKpiPublisher;
    }

    @RequestMapping(value="run", method= RequestMethod.GET, produces = "application/json")
    public void run(){
        ruleKpiPublisher.processRuleKpi();
    }
}
