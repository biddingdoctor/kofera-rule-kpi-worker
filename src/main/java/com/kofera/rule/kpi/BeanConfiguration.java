package com.kofera.rule.kpi;

import com.google.cloud.datastore.Datastore;
import com.google.cloud.datastore.DatastoreOptions;
import com.google.cloud.pubsub.v1.AckReplyConsumer;
import com.google.cloud.pubsub.v1.MessageReceiver;
import com.google.cloud.pubsub.v1.Subscriber;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.pubsub.v1.PubsubMessage;
import com.google.pubsub.v1.SubscriptionName;
import com.kofera.rule.kpi.task.RuleKpiSubscriber;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

import static com.kofera.rule.kpi.constant.GoogleCloudConstant.*;

@Configuration
public class BeanConfiguration {

    @Bean
    @Primary
    @ConfigurationProperties(prefix = "spring.datasource-adwords")
    public DataSource dataSourceAdwords() {
        return DataSourceBuilder.create().build();
    }

    @Bean
    JdbcTemplate jdbcTemplateAdwords(){
        return new JdbcTemplate(dataSourceAdwords());
    }

    @Bean
    Datastore datastore() {
        return DatastoreOptions.newBuilder().setProjectId(DS_PROJECT_ID).build().getService();
    }

    @Bean
    Storage storage() {
        return StorageOptions.newBuilder().setProjectId(ST_PROJECT_ID).build().getService();
    }

    @Bean(destroyMethod = "stopAsync")
    Subscriber subscriber(RuleKpiSubscriber ruleKpiSubscriber) {
        MessageReceiver receiver =
                new MessageReceiver() {
                    @Override
                    public void receiveMessage(PubsubMessage message, AckReplyConsumer consumer) {
                        ruleKpiSubscriber.subscribeRuleKpi(message, consumer);
                    }
                };
        Subscriber subscriber = null;
        SubscriptionName subscriptionName = SubscriptionName.create(PS_PROJECT_ID, PS_KPI_RULE_SUB);
        subscriber = Subscriber.defaultBuilder(subscriptionName, receiver).build();
//        task = Subscriber.newBuilder(subscriptionName, receiver).build();
        subscriber.addListener(
                new Subscriber.Listener() {
                    @Override
                    public void failed(Subscriber.State from, Throwable failure) {
                        // Handle failure. This is called when the Subscriber encountered a fatal error and is shutting down.
                        ruleKpiSubscriber.logError(failure);
                    }
                },
                MoreExecutors.directExecutor());
        subscriber.startAsync().awaitRunning();
        return subscriber;
    }
}
