package com.kofera.rule.kpi.dao;

import com.kofera.rule.kpi.domain.RuleKpi;

import java.util.Date;
import java.util.List;

public interface RuleKpiDao {
    RuleKpi findOneByRuleId(Integer ruleId);

    List<RuleKpi> findAll(Integer flag);

    List<Long> findAllCampaignByRuleId(Integer ruleId);

    List<Long> findAllConversionByRuleId(Integer ruleId);

    void updateNextRun(Integer ruleId, Date lastRunAt, Date nextRunAt);
}
