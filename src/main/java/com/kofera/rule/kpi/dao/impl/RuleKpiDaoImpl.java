package com.kofera.rule.kpi.dao.impl;

import com.google.cloud.Timestamp;
import com.google.cloud.datastore.*;
import com.kofera.rule.kpi.dao.RuleKpiDao;
import com.kofera.rule.kpi.domain.RuleKpi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.kofera.rule.kpi.constant.GoogleCloudConstant.*;
import static com.kofera.rule.kpi.dao.impl.DomainComposer.composeRuleKpi;
import static com.kofera.rule.kpi.utils.DatastoreUtils.longOf;

@Repository
public class RuleKpiDaoImpl implements RuleKpiDao {
    private Datastore datastore;
    private KeyFactory keyFactory;

    @Autowired
    public RuleKpiDaoImpl(Datastore datastore) {
        this.datastore = datastore;
        this.keyFactory = datastore.newKeyFactory().setKind(DS_KPI_SETTING_RULE);
    }

    @Override
    public RuleKpi findOneByRuleId(Integer ruleId) {
        try {
            Entity e = datastore.get(keyFactory.newKey(ruleId));
            return composeRuleKpi(e);
        } catch (NullPointerException e) {
            return null;
        }
    }

    @Override
    public List<RuleKpi> findAll(Integer flag) {
        List<RuleKpi> ruleKpiList = new ArrayList<>();
        try {
            Query<Entity> query = Query.newEntityQueryBuilder()
                    .setKind(DS_KPI_SETTING_RULE)
                    .setFilter(StructuredQuery.PropertyFilter.eq("runningFlag", flag))
                    .build();

            QueryResults<Entity> results = datastore.run(query);

            while (results.hasNext()) {
                Entity e = results.next();
                if (Timestamp.now().compareTo(e.getTimestamp("nextRunAt")) > 0) {
                    List<Long> campaignIds = findAllCampaignByRuleId(e.getKey().getId().intValue());
                    if (campaignIds.size() > 0) {
                        RuleKpi ruleKpi = composeRuleKpi(e);
                        ruleKpi.setCampaignIds(campaignIds);
                        ruleKpiList.add(ruleKpi);
                    }
                }
            }
        } catch (NullPointerException e) {

        }
        return ruleKpiList;
    }

    @Override
    public List<Long> findAllCampaignByRuleId(Integer ruleId) {
        List<Long> campaignIds = new ArrayList<>();
        try {
            Query<Entity> query = Query.newEntityQueryBuilder()
                    .setKind(DS_CAMPAIGN_RULE)
                    .setFilter(StructuredQuery.PropertyFilter.eq("parent", ruleId))
                    .build();

            QueryResults<Entity> results = datastore.run(query);

            while (results.hasNext()) {
                Entity e = results.next();
                campaignIds.add(longOf(e, "campaignId"));
            }
        } catch (NullPointerException e) {

        }
        return campaignIds;
    }

    @Override
    public List<Long> findAllConversionByRuleId(Integer ruleId) {
        List<Long> conversionTrackerIds = new ArrayList<>();
        try {
            Query<Entity> query = Query.newEntityQueryBuilder()
                    .setKind(DS_CONVERSION_TRACKER_RULE)
                    .setFilter(StructuredQuery.PropertyFilter.eq("parent", ruleId))
                    .build();

            QueryResults<Entity> results = datastore.run(query);

            while (results.hasNext()) {
                Entity e = results.next();
                conversionTrackerIds.add(longOf(e, "conversionTrackerId"));
            }
        } catch (NullPointerException e) {

        }
        return conversionTrackerIds;
    }

    @Override
    public void updateNextRun(Integer ruleId, Date lastRunAt, Date nextRunAt) {
        try {
            Entity entity = Entity.newBuilder(datastore.get(keyFactory.newKey(ruleId)))
                    .set("lastRunAt", Timestamp.of(lastRunAt))
                    .set("nextRunAt", Timestamp.of(nextRunAt))
                    .build();
            datastore.update(entity);
        } catch (NullPointerException e) {

        }
    }

}

