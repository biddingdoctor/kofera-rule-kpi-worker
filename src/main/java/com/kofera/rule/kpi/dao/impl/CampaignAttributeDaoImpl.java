package com.kofera.rule.kpi.dao.impl;

import com.kofera.rule.kpi.dao.CampaignAttributeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class CampaignAttributeDaoImpl implements CampaignAttributeDao {
    private JdbcTemplate jdbcTemplateAdwords;

    @Autowired
    public CampaignAttributeDaoImpl(JdbcTemplate jdbcTemplateAdwords) {
        this.jdbcTemplateAdwords = jdbcTemplateAdwords;
    }

    @Override
    public Long getBudget(Long accountId, Long campaignId) {
        String query = "select amount from campaign_attribute where account_id = ? and campaign_id = ?";
        Object[] params = new Object[] {accountId, campaignId};
        Long budget = jdbcTemplateAdwords.queryForObject(query, params, Long.class);
        return budget;
    }
}
