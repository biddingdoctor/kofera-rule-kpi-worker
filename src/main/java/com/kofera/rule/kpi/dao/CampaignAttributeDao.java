package com.kofera.rule.kpi.dao;

public interface CampaignAttributeDao {
    Long getBudget(Long accountId, Long campaignId);
}
