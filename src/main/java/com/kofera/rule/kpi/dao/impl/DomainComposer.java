package com.kofera.rule.kpi.dao.impl;

import com.google.cloud.datastore.Entity;
import com.google.cloud.datastore.FullEntity;
import com.kofera.rule.kpi.domain.Action;
import com.kofera.rule.kpi.domain.Condition;
import com.kofera.rule.kpi.domain.RuleKpi;

import static com.kofera.rule.kpi.utils.DatastoreUtils.*;
import static com.kofera.rule.kpi.utils.MapperUtils.mapToAction;
import static com.kofera.rule.kpi.utils.MapperUtils.mapToCondition;

public class DomainComposer {

    public static RuleKpi composeRuleKpi(Entity e) {
        String conditions = stringOf(e, "conditions");
        String action = stringOf(e, "action");

        RuleKpi r = new RuleKpi();
        r.setAccountId(longOf(e, "accountId"));
        r.setKoferaId(longOf(e, "koferaId"));
        r.setRuleId(e.getKey().getId().intValue());
        r.setConditions(mapToCondition(conditions));
        r.setAction(mapToAction(action));
        r.setMoveToCampaignId(longOf(e, "moveToCampaignId"));
        r.setMoveToCampaignAccountId(longOf(e, "moveToCampaignAccountId"));
        r.setDuring(integerOf(e, "during"));
        r.setDuringUnit(stringOf(e, "duringUnit"));
        r.setEvery(integerOf(e, "every"));
        r.setEveryUnit(stringOf(e, "everyUnit"));
        r.setMaxCpc(integerOf(e, "maxCpc"));
        r.setMinCpc(integerOf(e, "minCpc"));
        r.setConversionTrackerIds(longsOf(e, "conversionTrackerIds"));
        r.setCreatedAt(dateOf(e, "createdAt"));

        return r;
    }

    public static Action composeAction(FullEntity fe) {
        Action a = new Action();
        a.setType(stringOf(fe, "type"));
        a.setValue(stringOf(fe, "value"));
        a.setUnit(stringOf(fe, "unit"));
        return a;
    }

    public static Condition composeCondition(FullEntity fe) {
        Condition c = new Condition();
        c.setCondition1(stringOf(fe, "condition1"));
        c.setCompareOperator(stringOf(fe, "compareOperator"));
        c.setCondition2(stringOf(fe, "condition2"));
        c.setLogicOperator(stringOf(fe, "logicOperator"));
        return c;
    }
}
