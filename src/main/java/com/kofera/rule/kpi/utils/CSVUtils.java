package com.kofera.rule.kpi.utils;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class CSVUtils {
    private static String COMMA_DELIMITED = ",";

    public static void writeDataBasedOnHeader(String header, Map<String, String> data, BufferedWriter writer) {
        List<String> headNames = Arrays.asList(header.split(","));
        for (String headName : headNames) {
            try {
                switch (headName.trim()) {
                    case "accountId":
                        writer.write(String.valueOf(data.get("Customer ID")));
                        writer.write(COMMA_DELIMITED);
                        break;
                    case "campaignId":
                        writer.write(String.valueOf(data.get("Campaign ID")));
                        writer.write(COMMA_DELIMITED);
                        break;
                    case "adGroupId":
                        writer.write(String.valueOf(data.get("Ad group ID")));
                        writer.write(COMMA_DELIMITED);
                        break;
                    case "keywordId":
                        writer.write(String.valueOf(data.get("Keyword ID")));
                        writer.write(COMMA_DELIMITED);
                        break;
                    case "newStatus":
                        writer.write(String.valueOf(data.get("newStatus")));
                        writer.write(COMMA_DELIMITED);
                        break;
                    case "currentBid":
                        writer.write(String.valueOf(data.get("Max. CPC")));
                        writer.write(COMMA_DELIMITED);
                        break;
                    case "newBid":
                        writer.write(String.valueOf(data.get("newBid")));
                        writer.write(COMMA_DELIMITED);
                        break;
                    case "sourceBudget":
                        writer.write(String.valueOf(data.get("sourceBudget")));
                        writer.write(COMMA_DELIMITED);
                        break;
                    case "destinationAccountId":
                        writer.write(String.valueOf(data.get("destinationAccountId")));
                        writer.write(COMMA_DELIMITED);
                        break;
                    case "destinationCampaignId":
                        writer.write(String.valueOf(data.get("destinationCampaignId")));
                        writer.write(COMMA_DELIMITED);
                        break;
                    case "destinationBudget":
                        writer.write(String.valueOf(data.get("destinationBudget")));
                        writer.write(COMMA_DELIMITED);
                        break;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
