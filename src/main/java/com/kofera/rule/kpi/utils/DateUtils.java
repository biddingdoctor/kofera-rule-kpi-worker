package com.kofera.rule.kpi.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DateUtils {

    public static String composePartition(Date date) {
        String year = new SimpleDateFormat("yyyy").format(date);
        String month = new SimpleDateFormat("MM").format(date);
        String partition = "P" + year + month;
        return partition;
    }

    public static String composeDateTimePath(Date date) {
        String year = new SimpleDateFormat("yyyy").format(date);
        String month = new SimpleDateFormat("MM").format(date);
        String day = new SimpleDateFormat("dd").format(date);
        String hour = new SimpleDateFormat("HH").format(date);
        String datePath = year + "/" + month + "/" + day + "/" + hour;
        return datePath;
    }

    public static String composeDatePath(Date date) {
        String year = new SimpleDateFormat("yyyy").format(date);
        String month = new SimpleDateFormat("MM").format(date);
        String day = new SimpleDateFormat("dd").format(date);
        String datePath = year + "/" + month + "/" + day;
        return datePath;
    }

    public static String getFormattedDate(Date date, String sourceDataType) {
//        if (ActionConstant.SOURCE_KEYWORD.equals(sourceDataType)) {
            return new SimpleDateFormat("yyyy-MM-dd").format(date);
//        } else {
//            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
//        }
    }

    public static Date getEndDate(Date now) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(now);
        calendar.add(Calendar.DATE, -1);
        return calendar.getTime();
    }

    public static Date getStartDate(Date endDate, Integer during, String duringUnit) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(endDate);

        if ("MONTH".equals(duringUnit)) {
            calendar.add(Calendar.MONTH, -during);
        } else if ("WEEK".equals(duringUnit)) {
                calendar.add(Calendar.WEEK_OF_MONTH, -during);
        } else if ("DAY".equals(duringUnit)) {
            calendar.add(Calendar.DATE, -during);
        } else if ("HOUR".equals(duringUnit)) {
            calendar.add(Calendar.HOUR, -during);
        }

        return calendar.getTime();
    }

    public static List<String> composeSourcesPath(Date startDate, Date endDate, String sourceDataType, Long accountId) {
        List<String> sourcesPath = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);

        Calendar endCalendar = Calendar.getInstance();
        endCalendar.setTime(endDate);

        while (calendar.compareTo(endCalendar) <= 0) {
            Date result = calendar.getTime();
            String datePath = composeDatePath(result);
            String path = "gs://kofera-data-storage/raw/adwords/" + sourceDataType + "/" + datePath + "/" + accountId + ".csv";
            sourcesPath.add(path);
            calendar.add(Calendar.DATE, 1);
        }
        return sourcesPath;
    }

    public static Date getNextRun(Date now, Integer every, String everyUnit) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(now);

        if (everyUnit.equals("HOUR")) {
            calendar.add(Calendar.HOUR, every);
        } else if (everyUnit.equals("DAY")) {
            calendar.add(Calendar.DATE, every);
        } else if (everyUnit.equals("WEEK")) {
            calendar.add(Calendar.WEEK_OF_MONTH, every);
        } else if (everyUnit.equals("MONTH")) {
            calendar.add(Calendar.MONTH, every);
        }
        return calendar.getTime();
    }
}
