package com.kofera.rule.kpi.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kofera.rule.kpi.domain.Action;
import com.kofera.rule.kpi.domain.Condition;

import java.io.IOException;
import java.util.List;

public class MapperUtils {
    private static ObjectMapper mapper = new ObjectMapper();

    public static List<Condition> mapToCondition(String conditions) {
        try {
            List<Condition> conditionList = mapper.readValue(conditions, new TypeReference<List<Condition>>() {});
            return conditionList;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Action mapToAction(String action) {
        try {
            Action a = mapper.readValue(action, Action.class);
            return a;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


}
