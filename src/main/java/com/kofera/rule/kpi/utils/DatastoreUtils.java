package com.kofera.rule.kpi.utils;

import com.google.cloud.datastore.Entity;
import com.google.cloud.datastore.EntityValue;
import com.google.cloud.datastore.FullEntity;
import com.google.cloud.datastore.Value;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DatastoreUtils {
    public static String stringOf(Entity entity, String attribute) {
        if (entity.contains(attribute) && !entity.isNull(attribute)) {
            return entity.getString(attribute);
        }
        return null;
    }

    public static String stringOf(FullEntity fullEntity, String attribute) {
        if (fullEntity.contains(attribute) && !fullEntity.isNull(attribute)) {
            return fullEntity.getString(attribute);
        }
        return null;
    }

    public static Long longOf(Entity entity, String attribute) {
        if (entity.contains(attribute) && !entity.isNull(attribute)) {
            return entity.getLong(attribute);
        }
        return null;
    }

    public static Integer integerOf(Entity entity, String attribute) {
        if (entity.contains(attribute) && !entity.isNull(attribute)) {
            return (int) entity.getLong(attribute);
        }
        return null;
    }

    public static Boolean booleanOf(Entity entity, String attribute) {
        if (entity.contains(attribute) && !entity.isNull(attribute)) {
            return entity.getBoolean(attribute);
        }
        return false;
    }

    public static Date dateOf(Entity entity, String attribute) {
        if (entity.contains(attribute) && !entity.isNull(attribute)) {
            return new Date(entity.getTimestamp(attribute).getSeconds() * 1000);
        }
        return null;
    }

    public static FullEntity entityOf(Entity entity, String attribute) {
        if (entity.contains(attribute) && !entity.isNull(attribute)) {
            return entity.getEntity(attribute);
        }
        return null;
    }

    public static List<EntityValue> entityValuesOf(Entity entity, String attribute) {
        if (entity.contains(attribute) && !entity.isNull(attribute)) {
            return entity.getList(attribute);
        }
        return null;
    }

    public static List<Long> longsOf(Entity entity, String attribute) {
        if (entity.contains(attribute) && !entity.isNull(attribute)) {
            List<Long> longs = new ArrayList<>();
            for (Value sv : entity.getList(attribute)) {
                Long camapignId = Long.valueOf(sv.get().toString());
                longs.add(camapignId);
            }
            return longs;
        }
        return null;
    }
}
