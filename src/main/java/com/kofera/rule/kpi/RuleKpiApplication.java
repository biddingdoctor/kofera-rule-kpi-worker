package com.kofera.rule.kpi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableAsync
public class RuleKpiApplication {
    public static void main(String[] args) {
        SpringApplication.run(RuleKpiApplication.class, args);
    }
}
