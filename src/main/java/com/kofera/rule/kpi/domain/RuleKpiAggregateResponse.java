package com.kofera.rule.kpi.domain;

public class RuleKpiAggregateResponse {
    private Long accountId;
    private Integer ruleId;
    private String sourcePath;

    public RuleKpiAggregateResponse() {

    }

    public RuleKpiAggregateResponse(Long accountId, Integer ruleId, String sourcePath) {
        this.accountId = accountId;
        this.ruleId = ruleId;
        this.sourcePath = sourcePath;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Integer getRuleId() {
        return ruleId;
    }

    public void setRuleId(Integer ruleId) {
        this.ruleId = ruleId;
    }

    public String getSourcePath() {
        return sourcePath;
    }

    public void setSourcePath(String sourcePath) {
        this.sourcePath = sourcePath;
    }

    @Override
    public String toString() {
        return "RuleKpiAggregateResponse{" +
                "accountId=" + accountId +
                ", ruleId=" + ruleId +
                ", sourcePath='" + sourcePath + '\'' +
                '}';
    }
}
