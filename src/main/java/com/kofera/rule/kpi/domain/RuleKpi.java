package com.kofera.rule.kpi.domain;

import java.util.Date;
import java.util.List;

public class RuleKpi {
    private Long accountId;
    private Long koferaId;
    private Integer ruleId;
    private List<Condition> conditions;
    private Action action;
    private Long moveToCampaignId;
    private Long moveToCampaignAccountId;
    private Integer during;
    private String duringUnit;
    private Integer every;
    private String everyUnit;
    private Integer maxCpc;
    private Integer minCpc;
    private List<Long> campaignIds;
    private List<Long> conversionTrackerIds;
    private Date createdAt;

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Long getKoferaId() {
        return koferaId;
    }

    public void setKoferaId(Long koferaId) {
        this.koferaId = koferaId;
    }

    public Integer getRuleId() {
        return ruleId;
    }

    public void setRuleId(Integer ruleId) {
        this.ruleId = ruleId;
    }

    public List<Condition> getConditions() {
        return conditions;
    }

    public void setConditions(List<Condition> conditions) {
        this.conditions = conditions;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public Long getMoveToCampaignId() {
        return moveToCampaignId;
    }

    public void setMoveToCampaignId(Long moveToCampaignId) {
        this.moveToCampaignId = moveToCampaignId;
    }

    public Long getMoveToCampaignAccountId() {
        return moveToCampaignAccountId;
    }

    public void setMoveToCampaignAccountId(Long moveToCampaignAccountId) {
        this.moveToCampaignAccountId = moveToCampaignAccountId;
    }

    public Integer getDuring() {
        return during;
    }

    public void setDuring(Integer during) {
        this.during = during;
    }

    public String getDuringUnit() {
        return duringUnit;
    }

    public void setDuringUnit(String duringUnit) {
        this.duringUnit = duringUnit;
    }

    public Integer getEvery() {
        return every;
    }

    public void setEvery(Integer every) {
        this.every = every;
    }

    public String getEveryUnit() {
        return everyUnit;
    }

    public void setEveryUnit(String everyUnit) {
        this.everyUnit = everyUnit;
    }

    public Integer getMaxCpc() {
        return maxCpc;
    }

    public void setMaxCpc(Integer maxCpc) {
        this.maxCpc = maxCpc;
    }

    public Integer getMinCpc() {
        return minCpc;
    }

    public void setMinCpc(Integer minCpc) {
        this.minCpc = minCpc;
    }

    public List<Long> getCampaignIds() {
        return campaignIds;
    }

    public void setCampaignIds(List<Long> campaignIds) {
        this.campaignIds = campaignIds;
    }

    public List<Long> getConversionTrackerIds() {
        return conversionTrackerIds;
    }

    public void setConversionTrackerIds(List<Long> conversionTrackerIds) {
        this.conversionTrackerIds = conversionTrackerIds;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "RuleKpi{" +
                "accountId=" + accountId +
                ", koferaId=" + koferaId +
                ", ruleId=" + ruleId +
                ", conditions=" + conditions +
                ", action=" + action +
                ", moveToCampaignId=" + moveToCampaignId +
                ", moveToCampaignAccountId=" + moveToCampaignAccountId +
                ", during=" + during +
                ", duringUnit='" + duringUnit + '\'' +
                ", every=" + every +
                ", everyUnit='" + everyUnit + '\'' +
                ", maxCpc=" + maxCpc +
                ", minCpc=" + minCpc +
                ", campaignIds=" + campaignIds +
                ", conversionTrackerIds=" + conversionTrackerIds +
                ", createdAt=" + createdAt +
                '}';
    }
}
