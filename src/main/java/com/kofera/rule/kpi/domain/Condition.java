package com.kofera.rule.kpi.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Condition {

	@JsonProperty(value = "condition1")
	private String condition1;
	@JsonProperty(value = "compare_operator")
	private String compareOperator;
	@JsonProperty(value = "condition2")
	private String condition2;
	@JsonProperty(value = "logic_operator")
	private String logicOperator;

	public String getCondition1() {
		return condition1;
	}

	public void setCondition1(String condition1) {
		this.condition1 = condition1;
	}

	public String getCompareOperator() {
		return compareOperator;
	}

	public void setCompareOperator(String compareOperator) {
		this.compareOperator = compareOperator;
	}

	public String getCondition2() {
		return condition2;
	}

	public void setCondition2(String condition2) {
		this.condition2 = condition2;
	}

	public String getLogicOperator() {
		return logicOperator;
	}

	public void setLogicOperator(String logicOperator) {
		this.logicOperator = logicOperator;
	}

	@Override
	public String toString() {
		return "Condition{" +
				"condition1='" + condition1 + '\'' +
				", compareOperator='" + compareOperator + '\'' +
				", condition2='" + condition2 + '\'' +
				", logicOperator='" + logicOperator + '\'' +
				'}';
	}
}
