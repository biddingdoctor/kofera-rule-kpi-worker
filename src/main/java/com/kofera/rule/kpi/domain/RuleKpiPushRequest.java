package com.kofera.rule.kpi.domain;

public class RuleKpiPushRequest {
    private Long accountId;
    private String actionType;
    private String sourcePath;

    public RuleKpiPushRequest(Long accountId, String actionType, String sourcePath) {
        this.accountId = accountId;
        this.actionType = actionType;
        this.sourcePath = sourcePath;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getSourcePath() {
        return sourcePath;
    }

    public void setSourcePath(String sourcePath) {
        this.sourcePath = sourcePath;
    }

    @Override
    public String toString() {
        return "RuleKpiPushRequest{" +
                "accountId=" + accountId +
                ", actionType='" + actionType + '\'' +
                ", sourcePath='" + sourcePath + '\'' +
                '}';
    }
}
