package com.kofera.rule.kpi.domain;

import java.util.List;

public class RuleKpiAggregateRequest {
    private Long accountId;
    private Integer ruleId;
    private List<Long> campaignIds;
    private List<String> metrics;
    private String startDateTime;
    private String endDateTime;
    private List<String> sourcePath;
    private String destinationPath;
    private String destinationLevel;

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Integer getRuleId() {
        return ruleId;
    }

    public void setRuleId(Integer ruleId) {
        this.ruleId = ruleId;
    }

    public List<Long> getCampaignIds() {
        return campaignIds;
    }

    public void setCampaignIds(List<Long> campaignIds) {
        this.campaignIds = campaignIds;
    }

    public List<String> getMetrics() {
        return metrics;
    }

    public void setMetrics(List<String> metrics) {
        this.metrics = metrics;
    }

    public String getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
    }

    public String getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(String endDateTime) {
        this.endDateTime = endDateTime;
    }

    public List<String> getSourcePath() {
        return sourcePath;
    }

    public void setSourcePath(List<String> sourcePath) {
        this.sourcePath = sourcePath;
    }

    public String getDestinationPath() {
        return destinationPath;
    }

    public void setDestinationPath(String destinationPath) {
        this.destinationPath = destinationPath;
    }

    public String getDestinationLevel() {
        return destinationLevel;
    }

    public void setDestinationLevel(String destinationLevel) {
        this.destinationLevel = destinationLevel;
    }

    @Override
    public String toString() {
        return "RuleKpiAggregateRequest{" +
                "accountId=" + accountId +
                ", ruleId=" + ruleId +
                ", campaignIds=" + campaignIds +
                ", metrics=" + metrics +
                ", startDateTime='" + startDateTime + '\'' +
                ", endDateTime='" + endDateTime + '\'' +
                ", sourcePath=" + sourcePath +
                ", destinationPath='" + destinationPath + '\'' +
                ", destinationLevel='" + destinationLevel + '\'' +
                '}';
    }
}
