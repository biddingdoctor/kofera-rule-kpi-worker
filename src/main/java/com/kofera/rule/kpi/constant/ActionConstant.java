package com.kofera.rule.kpi.constant;

public class ActionConstant {
    public static final String ACTION_TYPE_PAUSE = "PAUSE";
    public static final String ACTION_TYPE_DELETE = "DELETE";
    public static final String ACTION_TYPE_MOVE_BUDGET = "MOVE BUDGET TO";
    public static final String ACTION_TYPE_INCREASE_BID = "INCREASED BID BY";
    public static final String ACTION_TYPE_DECREASE_BID = "DECREASE BID BY";

    public static final String ACTION_VALUE_CAMPAIGN = "CAMPAIGN";
    public static final String ACTION_VALUE_ADGROUP = "ADGROUP";
    public static final String ACTION_VALUE_KEYWORD = "KEYWORD";

    public static final String PAUSED = "PAUSED";
    public static final String REMOVED = "REMOVED";

    public static final String UPDATE_STATUS_CAMPAIGN = "UPDATE_STATUS_CAMPAIGN";
    public static final String UPDATE_STATUS_ADGROUP = "UPDATE_STATUS_ADGROUP";
    public static final String UPDATE_STATUS_KEYWORD = "UPDATE_STATUS_KEYWORD";
    public static final String CHANGE_BID = "CHANGE_BID_RULE";
    public static final String MOVE_CAMPAIGN_BUDGET = "MOVE_CAMPAIGN_BUDGET";

    public static final String SOURCE_CAMPAIGN = "campaign-performance-kpi";
    public static final String SOURCE_ADGROUP = "adgroup-performance-kpi";
    public static final String SOURCE_KEYWORD = "keyword-performance-kpi";

    public static final String DESTINATION_CAMPAIGN = "campaign-performance-kpi-rule";
    public static final String DESTINATION_ADGROUP = "adgroup-performance-kpi-rule";
    public static final String DESTINATION_KEYWORD = "keyword-performance-kpi-rule";

}
