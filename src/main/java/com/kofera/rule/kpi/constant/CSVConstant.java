package com.kofera.rule.kpi.constant;

public class CSVConstant {
    public static final String UPDATE_STATUS_CAMPAIGN_HEADER = "accountId,campaignId,newStatus";
    public static final String UPDATE_STATUS_ADGROUP_HEADER = "accountId,adGroupId,newStatus";
    public static final String UPDATE_STATUS_KEYWORD_HEADER = "accountId,keywordId,adGroupId,newStatus";
    public static final String CHANGE_BID_HEADER = "accountId,keywordId,adGroupId,currentBid,newBid";
    public static final String MOVE_CAMPAIGN_BUDGET_HEADER = "accountId,campaignId,sourceBudget,destinationAccountId,destinationCampaignId,destinationBudget";
}
