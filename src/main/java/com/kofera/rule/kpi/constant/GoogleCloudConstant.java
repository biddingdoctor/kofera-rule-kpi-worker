package com.kofera.rule.kpi.constant;

public class GoogleCloudConstant {
    //DS : DATASTORE
    public static final String DS_PROJECT_ID = "kofera-core-team";
    public static final String DS_KPI_SETTING_RULE = "kpi-setting-rule";
    public static final String DS_CAMPAIGN_RULE = "kpi-setting-rule-campaign";
    public static final String DS_CONVERSION_TRACKER_RULE = "kpi-setting-rule-conversion";

    //ST : STORAGE
    public static final String ST_PROJECT_ID = "kofera-technology-poc";
    public static final String ST_BUCKETNAME = "kofera-data-storage";

    //PS : PUBSUB
    public static final String PS_PROJECT_ID = "kofera-technology";
    public static final String PS_KPI_RULE_AGGREGATE = "kpi-rule-aggregate";
    public static final String PS_KPI_RULE_SUB = "kpi-rule-sub";

    public static final String PS_PROJECT_ID_POC = "kofera-technology-poc";
    public static final String PS_KPI_RULE_PUSH_TOPIC = "kpi-rule-push-topic";
}
