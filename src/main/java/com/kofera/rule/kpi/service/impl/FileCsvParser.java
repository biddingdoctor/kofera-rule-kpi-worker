package com.kofera.rule.kpi.service.impl;

import com.google.cloud.ReadChannel;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.Storage;
import com.opencsv.CSVReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.channels.Channels;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import static com.kofera.rule.kpi.constant.GoogleCloudConstant.*;

@Service
public class FileCsvParser {
    private static final Logger logger = LoggerFactory.getLogger(FileCsvParser.class.getName());
    private Storage storage;

    @Autowired
    public FileCsvParser(Storage storage) {
        this.storage = storage;
    }

    public void parseAndProcessAll(String sourcePath, Consumer<Map<String, String>> c) {
        String path = getPath(sourcePath);

        // Find delimiter
        char delimiter = ',';
        try {
            delimiter = findDelimiter(path);
        } catch (FileNotFoundException e) {
            logger.error("findDelimiter|{}", e);
            return;
        } catch (IOException e) {
            logger.error("findDelimiter|{}", e);
            return;
        }

        //Parse and Read
        try {
            parseAndProcessAll(path, delimiter, c);
        } catch (FileNotFoundException e) {
            logger.error("parseAndProcessAll|{}", e);
            return;
        } catch (IOException e) {
            logger.error("parseAndProcessAll|{}", e);
            return;
        }
    }

    private void parseAndProcessAll(String filePathName, char delimiter, Consumer<Map<String, String>> c) throws IOException {
        CSVReader reader = new CSVReader(new InputStreamReader(downloadFile(filePathName)), delimiter);
        String[] nextLine = reader.readNext();
        String[] headers = nextLine;
        Map<String, String> item = new HashMap<>();
        while ((nextLine = reader.readNext()) != null) {
            int index = 0;
            for (String column : headers) {
                item.put(column, nextLine[index]);
                index++;
            }
            c.accept(item);
            item.clear();
        }
        reader.close();
    }

    private char findDelimiter(String filePathName) throws IOException {
        char[] delimiterCandidates = new char[]{',', '\t', ':', '|', ';'};
        for (char delimiter : delimiterCandidates) {
            CSVReader reader = new CSVReader(new InputStreamReader(downloadFile(filePathName)));
            String[] nextLine = reader.readNext();
            int colCount = nextLine.length;
            boolean delimiterCheck = true;
            int lineCheck = 0;
            while ((nextLine = reader.readNext()) != null && lineCheck < 20) {
                int lineColumnCount = nextLine.length;
                if (lineColumnCount == colCount) {
                    delimiterCheck = true;
                } else {
                    delimiterCheck = false;
                }
                lineCheck++;
            }
            if (delimiterCheck) {
                logger.info("findDelimiter|delimiter found : {}", delimiter);
                return delimiter;
            }
            reader.close();
        }
        //Delimeter not found
        logger.info("findDelimiter|delimiter not found");
        return ',';
    }

    private InputStream downloadFile(String sourceUrl) throws IOException {
        Blob blob = storage.get(ST_BUCKETNAME, sourceUrl);
        ReadChannel readChannel = blob.reader();
        InputStream is = Channels.newInputStream(readChannel);
        return is;
    }

    private String getPath(String sourcePath) {
        String[] arr = sourcePath.split("/" + ST_BUCKETNAME + "/");
        return arr[1];
    }

}
