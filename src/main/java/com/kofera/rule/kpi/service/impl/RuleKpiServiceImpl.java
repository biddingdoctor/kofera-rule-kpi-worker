package com.kofera.rule.kpi.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.cloud.pubsub.v1.Publisher;
import com.google.cloud.storage.Acl;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.kofera.rule.kpi.constant.CSVConstant;
import com.kofera.rule.kpi.constant.GoogleCloudConstant;
import com.kofera.rule.kpi.dao.CampaignAttributeDao;
import com.kofera.rule.kpi.dao.RuleKpiDao;
import com.kofera.rule.kpi.domain.Condition;
import com.kofera.rule.kpi.domain.RuleKpi;
import com.kofera.rule.kpi.domain.RuleKpiAggregateResponse;
import com.kofera.rule.kpi.domain.RuleKpiPushRequest;
import com.kofera.rule.kpi.service.RuleKpiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import static com.kofera.rule.kpi.constant.ActionConstant.*;
import static com.kofera.rule.kpi.utils.CSVUtils.writeDataBasedOnHeader;
import static com.kofera.rule.kpi.utils.DateUtils.composeDateTimePath;

@Service
public class RuleKpiServiceImpl implements RuleKpiService {
    private static Logger logger = LoggerFactory.getLogger(RuleKpiServiceImpl.class);
    private static ObjectMapper mapper = new ObjectMapper();

    private RuleKpiDao ruleKpiDao;
    private CampaignAttributeDao campaignAttributeDao;
    private FileCsvParser fileCsvParser;
    private Storage storage;

    @Autowired
    public RuleKpiServiceImpl(RuleKpiDao ruleKpiDao,
                              CampaignAttributeDao campaignAttributeDao,
                              FileCsvParser fileCsvParser,
                              Storage storage) {
        this.ruleKpiDao = ruleKpiDao;
        this.campaignAttributeDao = campaignAttributeDao;
        this.fileCsvParser = fileCsvParser;
        this.storage = storage;
    }

    @Override
    public void checkRule(RuleKpiAggregateResponse response) {
        logger.info("checkRule|{}", response);

        RuleKpi ruleKpi = ruleKpiDao.findOneByRuleId(response.getRuleId());
        logger.info("checkRule|{}", ruleKpi);

        String actionType = ruleKpi.getAction().getType();
        String actionValue = ruleKpi.getAction().getValue();
        String header = null;
        String actionTypePush = null;

        if (actionValue.equals(ACTION_VALUE_CAMPAIGN) && (actionType.equals(ACTION_TYPE_PAUSE) || actionType.equals(ACTION_TYPE_DELETE))) {
            header = CSVConstant.UPDATE_STATUS_CAMPAIGN_HEADER;
            actionTypePush = UPDATE_STATUS_CAMPAIGN;
        } else if (actionValue.equals(ACTION_VALUE_ADGROUP) && (actionType.equals(ACTION_TYPE_PAUSE) || actionType.equals(ACTION_TYPE_DELETE))) {
            header = CSVConstant.UPDATE_STATUS_ADGROUP_HEADER;
            actionTypePush = UPDATE_STATUS_ADGROUP;
        } else if (actionValue.equals(ACTION_VALUE_KEYWORD) && (actionType.equals(ACTION_TYPE_PAUSE) || actionType.equals(ACTION_TYPE_DELETE))) {
            header = CSVConstant.UPDATE_STATUS_KEYWORD_HEADER;
            actionTypePush = UPDATE_STATUS_KEYWORD;
        } else if (actionType.equals(ACTION_TYPE_INCREASE_BID) || actionType.equals(ACTION_TYPE_DECREASE_BID)) {
            header = CSVConstant.CHANGE_BID_HEADER;
            actionTypePush = CHANGE_BID;
        } else if (actionType.equals(ACTION_TYPE_MOVE_BUDGET)) {
            header = CSVConstant.MOVE_CAMPAIGN_BUDGET_HEADER;
            actionTypePush = MOVE_CAMPAIGN_BUDGET;
        }

        checkAndExportToCsv(response, ruleKpi, actionTypePush, header);

    }

    private void checkAndExportToCsv(RuleKpiAggregateResponse response, RuleKpi ruleKpi, String actionTypePush, String header) {
        logger.info("checkAndExportToCsv|ruleId={}|sourcePath={}", ruleKpi.getRuleId(), response.getSourcePath());
        File tempFile = null;
        try {
            tempFile = File.createTempFile(String.valueOf(ruleKpi.getRuleId()), ".csv");

            // Write data to the file
            checkAndWriteData(response, ruleKpi, actionTypePush, header, tempFile);

            boolean isNotNull = dataIsNotNull(tempFile);
            if (isNotNull == true) {
                //Upload file to google storage
                String path = uploadCsvAndGetPath(ruleKpi, actionTypePush, tempFile);

                if (path != null) {
                    //Compose request and publish
                    RuleKpiPushRequest pushRequest = new RuleKpiPushRequest(ruleKpi.getAccountId(), actionTypePush, path);
                    publishPushRequest(pushRequest);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (tempFile.exists()) {
                tempFile.delete();
            }
        }
    }

    private void checkAndWriteData(RuleKpiAggregateResponse response, RuleKpi ruleKpi, String actionTypePush, String header,
                                   File fileTemp) {
        logger.info("checkAndWriteData|ruleId={}|sourcePath={}", ruleKpi.getRuleId(), response.getSourcePath());
        BufferedWriter writer = null;

//        final Long destinationBudget = getBudget(ruleKpi, actionTypePush);

        try {
            writer = new BufferedWriter(new FileWriter(fileTemp));

            // write header
            writer.write(header);
            writer.newLine();

            // check & write data
            BufferedWriter finalWriter = writer;
            fileCsvParser.parseAndProcessAll(response.getSourcePath(), dtItem -> {
                boolean result = checkToCondition(ruleKpi, dtItem);
                if (result == true) {
                    if (actionTypePush.equals(UPDATE_STATUS_CAMPAIGN) || actionTypePush.equals(UPDATE_STATUS_ADGROUP) || actionTypePush.equals(UPDATE_STATUS_KEYWORD)) {
                        dtItem = processUpdateStatus(ruleKpi, dtItem);
                    } else if (actionTypePush.equals(CHANGE_BID)) {
                        dtItem = processChangeBid(ruleKpi, dtItem);
                    } else if (actionTypePush.equals(MOVE_CAMPAIGN_BUDGET)) {
                        dtItem.put("destinationAccountId", String.valueOf(ruleKpi.getMoveToCampaignAccountId()));
                        dtItem.put("destinationCampaignId", String.valueOf(ruleKpi.getMoveToCampaignId()));
//                        dtItem.put("destinationBudget", String.valueOf(destinationBudget));
//                        dtItem = processMoveBudget(ruleKpi, dtItem);
                    }

                    try {
                        writeDataBasedOnHeader(header, dtItem, finalWriter);
                        finalWriter.newLine();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private boolean checkToCondition(RuleKpi ruleKpi, Map<String, String> dtItem) {
        boolean result = false;
        boolean comparator = false;
        String logicOperator = null;
        Double value;

        for (Condition c : ruleKpi.getConditions()) {
            value = getValue(c.getCondition1(), dtItem);
            if (value != null) {
                result = checkAndOperate(value, c);
                if (logicOperator != null) {
                    result = checkAndOperate(comparator, result, logicOperator);
                    logicOperator = null;
                }

                if (c.getLogicOperator() != null || !c.getLogicOperator().equals("")) {
                    logicOperator = c.getLogicOperator();
                    comparator = result;
                }
            }
        }
        return result;
    }

    private boolean checkAndOperate(Double a, Condition c) {
        boolean result = false;
        Double b = Double.valueOf(c.getCondition2());

        if (("<").equals(c.getCompareOperator())) {
            result = a < b;
        } else if (("<=").equals(c.getCompareOperator())) {
            result = a <= b;
        } else if ((">").equals(c.getCompareOperator())) {
            result = a > b;
        } else if ((">=").equals(c.getCompareOperator())) {
            result = a >= b;
        } else if (("==").equals(c.getCompareOperator())) {
            result = a == b;
        }

        return result;
    }

    private boolean checkAndOperate(boolean a, boolean b, String logic) {
        boolean result = false;
        if ("AND".equalsIgnoreCase(logic)) {
            result = a && b;
        } else if ("OR".equalsIgnoreCase(logic)) {
            result = a || b;
        }

        return result;
    }

    private Double getValue(String metric, Map<String, String> dtItem) {
        String columnName = null;
        Double value = null;
        if ("CPC".equals(metric)) {
            columnName = "Avg. CPC";
            value = getMicroAmountValue(Double.valueOf(dtItem.get(columnName)), dtItem);
        } else if ("CPO".equals(metric)) {
            columnName = "Cost / conv.";
        } else if ("CR".equals(metric)) {
            columnName = "Conv. rate";
        } else if ("IMPRESSION".equals(metric)) {
            columnName = "Impressions";
        } else if ("IMPRESSION_SHARE".equals(metric)) {
            columnName = "Search Impr. share";
        } else if ("CONTENT_IMPRESSION_SHARE".equals(metric)) {
            columnName = "Content Impr. share";
        } else if ("POSITION".equals(metric)) {
            columnName = "Avg. position";
        } else if ("CTR".equals(metric)) {
            columnName = "CTR";
        } else if ("CLICKS".equals(metric)) {
            columnName = "Clicks";
        } else if ("QS".equals(metric)) {
            columnName = "Quality score";
        } else if ("CONVERSION".equals(metric)) {
            columnName = "Conversions";
        }

        if (columnName != null && value == null) {
            value = Double.valueOf(dtItem.get(columnName));
        }

        return value;
    }

    private Long getBudget(RuleKpi ruleKpi, String actionTypePush) {
        if (MOVE_CAMPAIGN_BUDGET.equals(actionTypePush)) {
            return campaignAttributeDao.getBudget(ruleKpi.getMoveToCampaignAccountId(), ruleKpi.getMoveToCampaignId());
        } else {
            return null;
        }
    }

    private Map<String, String> processUpdateStatus(RuleKpi ruleKpi, Map<String, String> selectedData) {
        if (ruleKpi.getAction().getType().equals(ACTION_TYPE_PAUSE)) {
            selectedData.put("newStatus", PAUSED);
        } else {
            selectedData.put("newStatus", REMOVED);
        }
        return selectedData;
    }

    private Map<String, String> processChangeBid(RuleKpi ruleKpi, Map<String, String> selectedData) {
        String actionType = ruleKpi.getAction().getType();
        Double actionValue = Double.valueOf(ruleKpi.getAction().getValue());
        Integer maxCpc = ruleKpi.getMaxCpc();
        Integer minCpc = ruleKpi.getMinCpc();

        Integer multiple = 1;
        Double newBid;
        Double currentBid = Double.valueOf(selectedData.get("Max. CPC"));
//        String currencyCode = selectedData.get("Currency");
//        Integer bidRounded;

        if (actionType.equals(ACTION_TYPE_DECREASE_BID)) {
            multiple = -1;
        }

        if (("PERCENT").equals(ruleKpi.getAction().getUnit())) {
            newBid = currentBid * (100 + (actionValue * multiple)) / 100;
        } else {
            newBid = getMicroAmountValue(actionValue, selectedData);
            newBid = currentBid + (newBid * multiple);
        }

//        if (("IDR").equals(currencyCode)) {
//            bidRounded = (int) (newBid / 100000000);
//            newBid = (long) bidRounded * 100000000;
//        } else if (("USD").equals(currencyCode) || ("SGD").equals(currencyCode)) {
//            bidRounded = (int) (newBid / 10000);
//            newBid = (long) bidRounded * 10000;
//        }

        if (maxCpc != null) {
            newBid = (newBid <= maxCpc || maxCpc == 0) ? newBid : maxCpc;
        }
        if (minCpc != null) {
            newBid = (newBid >= minCpc || minCpc == 0) ? newBid : minCpc;
        }
        selectedData.put("newBid", String.valueOf(newBid));

        return selectedData;
    }

    private Map<String, String> processMoveBudget(RuleKpi ruleKpi, Map<String, String> selectedData) {
        Long sourceAccountId = Long.valueOf(selectedData.get("Customer ID"));
        Long sourceCampaignId = Long.valueOf(selectedData.get("Campaign ID"));
        Long sourceBudget = campaignAttributeDao.getBudget(sourceAccountId, sourceCampaignId);

        selectedData.put("sourceBudget", String.valueOf(sourceBudget));
        return selectedData;
    }

    private String uploadCsvAndGetPath(RuleKpi ruleKpi, String actionTypePush, File file) {
        Date date = new Date();
        String path = "kpi-rule-result/adwords/" + actionTypePush.toLowerCase() + "/" + composeDateTimePath(date) + "/" +
                ruleKpi.getAccountId() + "/" + ruleKpi.getRuleId() + ".csv";
        try {
            InputStream is = new FileInputStream(file.getAbsolutePath());
            BlobInfo blobInfo =
                    storage.create(
                            BlobInfo
                                    .newBuilder(GoogleCloudConstant.ST_BUCKETNAME, path)
                                    .setAcl(new ArrayList<>(Arrays.asList(Acl.of(Acl.User.ofAllUsers(), Acl.Role.READER))))
                                    .build(),
                            is);
            String url = blobInfo.getMediaLink();

            if (url != null) {
                logger.info("uploadCsvAndGetPath|ruleId={}|destinationPath={}", ruleKpi.getRuleId(), path);
                String sourcePath = "gs://kofera-data-storage/" +  path;
                return sourcePath;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    private boolean dataIsNotNull(File file) {
        try {
            InputStream is = new FileInputStream(file.getAbsolutePath());
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String header = reader.readLine();
            if (reader.readLine() != null) {
                return true;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    private Double getMicroAmountValue(Double number, Map<String, String> dtItem) {
        if ("IDR".equals(dtItem.get("Currency"))) {
            return number * 1000000;
        } else {
            return number;
        }
    }

    private void publishPushRequest(RuleKpiPushRequest pushRequest) {
        Publisher publisher = null;
        try {
            String message = mapper.writeValueAsString(pushRequest);
            logger.info("publishPushRequest|message{}", message);
//            ByteString data = ByteString.copyFrom(message.getBytes());
//            PubsubMessage pubsubMessage = PubsubMessage.newBuilder().setData(data).build();
//            TopicName topicName = TopicName.create(GoogleCloudConstant.PS_PROJECT_ID_POC, GoogleCloudConstant.PS_KPI_RULE_PUSH_TOPIC);
////            TopicName topicName = TopicName.of(GoogleCloudConstant.PS_PROJECT_ID, GoogleCloudConstant.PS_KPI_RULE_PUSH_TOPIC);
//            publisher = Publisher.defaultBuilder(topicName).build();
////            publisher = Publisher.newBuilder(topicName).build();
//            publisher.publish(pubsubMessage).get();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
//        } catch (IOException | InterruptedException | ExecutionException e) {
//            e.printStackTrace();
        }
    }

}
