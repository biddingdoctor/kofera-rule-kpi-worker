package com.kofera.rule.kpi.service;

import com.kofera.rule.kpi.domain.RuleKpiAggregateResponse;

public interface RuleKpiService {
    void checkRule(RuleKpiAggregateResponse response);
}
