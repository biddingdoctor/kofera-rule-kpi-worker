package com.kofera.rule.kpi.task;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.cloud.pubsub.v1.AckReplyConsumer;
import com.google.pubsub.v1.PubsubMessage;
import com.kofera.rule.kpi.domain.RuleKpiAggregateResponse;
import com.kofera.rule.kpi.service.RuleKpiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class RuleKpiSubscriber {
    private static Logger logger = LoggerFactory.getLogger(RuleKpiAggregateResponse.class);
    private static ObjectMapper mapper = new ObjectMapper();

    private RuleKpiService ruleKpiService;

    @Autowired
    public RuleKpiSubscriber(RuleKpiService ruleKpiService) { this.ruleKpiService = ruleKpiService; }

    @Async("asyncTaskExecutor")
    public void subscribeRuleKpi(PubsubMessage message, AckReplyConsumer consumer) {
        try {
            byte[] bytes = message.getData().toByteArray();
            RuleKpiAggregateResponse response = mapper.readValue(bytes, RuleKpiAggregateResponse.class);
            logger.info("subscribeRuleKpi|{}", response.toString());
            consumer.ack();

            ruleKpiService.checkRule(response);
        } catch (Throwable e) {
            logger.error("subscribeRuleKpi error|{}", e.toString());
        }
    }

    public void logError(Throwable throwable) {
        logger.error("logError|{}", throwable.toString());
    }
}
