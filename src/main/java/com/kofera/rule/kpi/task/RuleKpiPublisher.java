package com.kofera.rule.kpi.task;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.cloud.pubsub.v1.Publisher;
import com.google.protobuf.ByteString;
import com.google.pubsub.v1.PubsubMessage;
import com.google.pubsub.v1.TopicName;
import com.kofera.rule.kpi.dao.RuleKpiDao;
import com.kofera.rule.kpi.domain.Action;
import com.kofera.rule.kpi.domain.Condition;
import com.kofera.rule.kpi.domain.RuleKpi;
import com.kofera.rule.kpi.domain.RuleKpiAggregateRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static com.kofera.rule.kpi.constant.ActionConstant.*;
import static com.kofera.rule.kpi.constant.ConditionConstant.CONDITION1_QS;
import static com.kofera.rule.kpi.constant.GoogleCloudConstant.PS_KPI_RULE_AGGREGATE;
import static com.kofera.rule.kpi.constant.GoogleCloudConstant.PS_PROJECT_ID;
import static com.kofera.rule.kpi.utils.DateUtils.*;

@Component
public class RuleKpiPublisher {
    public static Logger logger = LoggerFactory.getLogger(RuleKpiPublisher.class);
    public static ObjectMapper mapper = new ObjectMapper();

    private RuleKpiDao ruleKpiDao;

    @Autowired
    public RuleKpiPublisher(RuleKpiDao ruleKpiDao) {
        this.ruleKpiDao = ruleKpiDao;
    }


    @Scheduled(fixedRateString = "${fixedDelayHourly.in.milliseconds}")
    public void processRuleKpi() {
        Integer runningFlag = 1;
        List<RuleKpi> ruleKpiList = ruleKpiDao.findAll(runningFlag);
        ruleKpiList.stream().forEach(ruleKpi -> {
            Date now = new Date();
            logger.info("processRuleKpi|ruleId={}", ruleKpi.getRuleId());

            RuleKpiAggregateRequest r = composeAggregateRequest(ruleKpi, now);
            logger.info("processRuleKpi|{}", r);

            if (r != null) {
                publishAggregateRequest(r);
            }

            Date nextRunAt = getNextRun(now, ruleKpi.getEvery(), ruleKpi.getEveryUnit());
            ruleKpiDao.updateNextRun(ruleKpi.getRuleId(), now, nextRunAt);
        });
    }

    public void processOneRuleKpi(Integer ruleId) {
        RuleKpi ruleKpiData = ruleKpiDao.findOneByRuleId(ruleId);
        ruleKpiData.setCampaignIds(ruleKpiDao.findAllCampaignByRuleId(ruleId));

        List<RuleKpi> ruleKpiList = Arrays.asList(ruleKpiData);
        ruleKpiList.stream().forEach(ruleKpi -> {
            Date now = new Date();
            logger.info("processRuleKpi|ruleId={}", ruleKpi.getRuleId());

            RuleKpiAggregateRequest r = composeAggregateRequest(ruleKpi, now);
            logger.info("processRuleKpi|{}", r);

            if (r != null) {
                publishAggregateRequest(r);
            }

            Date nextRunAt = getNextRun(now, ruleKpi.getEvery(), ruleKpi.getEveryUnit());
            ruleKpiDao.updateNextRun(ruleKpi.getRuleId(), now, nextRunAt);
        });
    }

    public RuleKpiAggregateRequest composeAggregateRequest(RuleKpi ruleKpi, Date now) {
        String destinationLevel = getDestinationLevel(ruleKpi);
        String sourceDataType = getSourceDataType(ruleKpi, destinationLevel);

        if (sourceDataType == null) {
            return null;
        }

        Date endDate = getEndDate(now);
        Date startDate = getStartDate(endDate, ruleKpi.getDuring(), ruleKpi.getDuringUnit());

        RuleKpiAggregateRequest r = new RuleKpiAggregateRequest();
        r.setAccountId(ruleKpi.getAccountId());
        r.setRuleId(ruleKpi.getRuleId());
        r.setCampaignIds(ruleKpi.getCampaignIds());
        r.setMetrics(composeMetric(ruleKpi, sourceDataType));
        r.setStartDateTime(getFormattedDate(startDate, sourceDataType));
        r.setEndDateTime(getFormattedDate(endDate, sourceDataType));
        r.setSourcePath(composeSourcesPath(startDate, endDate, sourceDataType, ruleKpi.getAccountId()));
        r.setDestinationPath(getDestinationPath(ruleKpi, destinationLevel, now));
        r.setDestinationLevel(destinationLevel);
        return r;
    }

    private String getDestinationLevel(RuleKpi ruleKpi) {
        Action action = ruleKpi.getAction();
        //checking for move campaign budget
        if (ACTION_VALUE_CAMPAIGN.equals(action.getValue()) &&
                ACTION_TYPE_MOVE_BUDGET.equals(action.getType()) &&
                ruleKpi.getMoveToCampaignId() != null) {
            return action.getValue();
        }
        //checking for update status
        else if (ACTION_VALUE_CAMPAIGN.equals(action.getValue()) || ACTION_VALUE_ADGROUP.equals(action.getValue()) || ACTION_VALUE_KEYWORD.equals(action.getValue())) {
            if (ACTION_TYPE_PAUSE.equals(action.getType()) || ACTION_TYPE_DELETE.equals(action.getType())) {
                return action.getValue();
            }
        }
        //checking for change bid
        else if (ACTION_TYPE_INCREASE_BID.equals(action.getType()) || ACTION_TYPE_DECREASE_BID.equals(action.getType())) {
            if (action.getValue() != null) {
                return ACTION_VALUE_KEYWORD;
            }
        }
        return null;
    }

    private String getSourceDataType(RuleKpi ruleKpi, String destinationLevel) {
        if (destinationLevel != null) {
            String sourceDataType = null;
            if (isContainQS(ruleKpi) == true) {
                sourceDataType = SOURCE_KEYWORD;
            } else if (destinationLevel.equals(ACTION_VALUE_CAMPAIGN)) {
                sourceDataType = SOURCE_CAMPAIGN;
            } else if (destinationLevel.equals(ACTION_VALUE_ADGROUP)) {
                sourceDataType = SOURCE_ADGROUP;
            } else if (destinationLevel.equals(ACTION_VALUE_KEYWORD)) {
                sourceDataType = SOURCE_KEYWORD;
            }
            return sourceDataType;
        }
        return null;
    }

    private boolean isContainQS(RuleKpi ruleKpi) {
        List<Condition> conditions = ruleKpi.getConditions();
        return conditions.stream().filter(o -> o.getCondition1().equals(CONDITION1_QS)).findFirst().isPresent();
    }

    private List<String> composeMetric(RuleKpi ruleKpi, String sourceDataType) {
        List<String> conditions = new ArrayList<>();
        for (Condition c : ruleKpi.getConditions()) {
            conditions.add(c.getCondition1());
        }

        if (SOURCE_KEYWORD.equals(sourceDataType)) {
            conditions.add("Max. CPC");
        }

        return conditions;
    }

    private String getDestinationPath(RuleKpi ruleKpi, String destinationLevel, Date now) {
        String destinationDataType = null;
        String destinationPath = null;
        if (destinationLevel.equals(ACTION_VALUE_CAMPAIGN)) {
            destinationDataType = DESTINATION_CAMPAIGN;
        } else if (destinationLevel.equals(ACTION_VALUE_ADGROUP)) {
            destinationDataType = DESTINATION_ADGROUP;
        } else if (destinationLevel.equals(ACTION_VALUE_KEYWORD)) {
            destinationDataType = DESTINATION_KEYWORD;
        }

        if (destinationDataType != null) {
            destinationPath = "gs://kofera-data-storage/kpi-rule/adwords/" + destinationDataType + "/" +
                    composeDateTimePath(now) + "/" + ruleKpi.getAccountId() + "/" + ruleKpi.getRuleId() + ".csv";
        }
        return destinationPath;
    }

    private void publishAggregateRequest(RuleKpiAggregateRequest aggregateRequest) {
        Publisher publisher = null;
        try {
            String message = mapper.writeValueAsString(aggregateRequest);
            logger.info("publishAggregateRequest|message{}", message);
            ByteString data = ByteString.copyFrom(message.getBytes());
            PubsubMessage pubsubMessage = PubsubMessage.newBuilder().setData(data).build();
            TopicName topicName = TopicName.create(PS_PROJECT_ID, PS_KPI_RULE_AGGREGATE);
            publisher = Publisher.defaultBuilder(topicName).build();
            publisher.publish(pubsubMessage).get();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException | InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }
}
