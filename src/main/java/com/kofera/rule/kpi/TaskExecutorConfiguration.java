package com.kofera.rule.kpi;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Configuration
public class TaskExecutorConfiguration {

    @Bean(destroyMethod = "shutdown")
    public ThreadPoolExecutor asyncTaskExecutor(
            @Value("${async_task_executor.poolSize:10}") int poolSize,
            @Value("${async_task_executor.queueCapacity:1}") int queueCapacity) {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(
                poolSize,
                poolSize,
                1000,
                TimeUnit.MILLISECONDS,
                new SynchronousQueue(),
                new ThreadPoolExecutor.CallerRunsPolicy());
        return threadPoolExecutor;
    }
}
