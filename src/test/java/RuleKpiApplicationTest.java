import com.kofera.rule.kpi.RuleKpiApplication;
import com.kofera.rule.kpi.dao.CampaignAttributeDao;
import com.kofera.rule.kpi.domain.RuleKpiAggregateResponse;
import com.kofera.rule.kpi.service.RuleKpiService;
import com.kofera.rule.kpi.task.RuleKpiPublisher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RuleKpiApplication.class)
public class RuleKpiApplicationTest {
//    @Autowired
    RuleKpiPublisher ruleKpiPublisher;

//    @Autowired
    RuleKpiService ruleKpiService;

//    @Autowired
    CampaignAttributeDao campaignAttributeDao;

    @Test
    public void test() throws IOException {

    }

//    @Test
    public void testPublisher() {
        ruleKpiPublisher.processOneRuleKpi(308);
    }

//    @Test
    public void testSubscriber() {
        RuleKpiAggregateResponse r = new RuleKpiAggregateResponse(
                7997871095L,
                306,
                "gs://kofera-data-storage/kpi-rule/adwords/keyword-performance-kpi-rule/2018/02/09/16/7997871095/306.csv");
        ruleKpiService.checkRule(r);
    }

//    @Test
    public void testCampaignAttributeDao() {
        Long amount = campaignAttributeDao.getBudget(1005041941L, 728056539L);
        System.out.println(amount);
    }
}
